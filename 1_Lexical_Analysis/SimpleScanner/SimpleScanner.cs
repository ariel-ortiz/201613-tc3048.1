﻿using System;
using System.IO;
using System.Text.RegularExpressions;

public class SimpleScanner
{
    public static void Main ()
    {
        var regex = new Regex (@"(/[*].*?[*]/)|(.)", RegexOptions.Singleline);
        var input = File.ReadAllText ("/home/aortiz/tc3048/git/1_Lexical_Analysis/SimpleScanner/test.c");
        foreach (Match m in regex.Matches(input)) {
            if (m.Groups [2].Length != 0) {
                Console.Write (m.Groups [2].Value);
            }
        }
    }
}
