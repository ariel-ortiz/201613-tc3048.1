
using System;

public class DynamicExample {
  
    public static void m(int a) {
        Console.WriteLine("I'm an int: " + a);
    }
    
    public static void m(String a) {
        Console.WriteLine("I'm a string: " + a);
    }
    
    public static void Main() {
        dynamic x = 5;
        m(x);
        x = "Thanks for all the fish!";
        m(x);
    }
}
