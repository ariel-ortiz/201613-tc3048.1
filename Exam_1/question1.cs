//---------------------------------------------------------
// Solution to problem 1.
//---------------------------------------------------------

using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Exam1 {
    class Question1 {
        public static void Main(String[] args) {

            if (args.Length != 1) {
                Console.WriteLine("You must specify the input file!");
                Environment.Exit(1);
            }

            var input = File.ReadAllText(args[0]);

            Console.WriteLine("{0} {1} {2}",
                Regex.Matches(input, @"\n").Count,
                Regex.Matches(input, @"\w+").Count,
                Regex.Matches(input, @".", RegexOptions.Singleline).Count
            );
        }
    }
}
