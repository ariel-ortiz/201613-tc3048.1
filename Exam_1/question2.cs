//---------------------------------------------------------
// Solution to problem 2.
//---------------------------------------------------------

using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Exam1 {
    class Question2 {
        public static void Main(String[] args) {

            if (args.Length != 1) {
                Console.WriteLine("You must specify the input file!");
                Environment.Exit(1);
            }

            var input = File.ReadAllText(args[0]);

            var regex = new Regex(@"0[bB][01]+(_*[01])*[lL]?\b");

            foreach (Match m in regex.Matches(input)) {
                Console.WriteLine(m.Value);
            }
        }
    }
}
