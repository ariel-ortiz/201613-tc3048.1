//---------------------------------------------------------
// Solution to problem 2.
//---------------------------------------------------------

%{

#include <stdio.h>
#include <stdarg.h>

void yyerror(char *s, ...);
extern int yylineno;
extern int yylex(void);

%}

%union {
    int ival;
}

/* declare tokens */
%token ADD MUL PAR_LEFT PAR_RIGHT EOL
%token<ival> INTEGER
%type<ival> calclist exp add_exp_list mul_exp_list

%%

calclist:
    /* nothing */ { }                            /* matches at beginning of input */
    | calclist exp EOL { printf("%d\n> ", $2); } /* EOL is end of an expression */
;

exp:
    INTEGER /* default $$ = $1 */
    | PAR_LEFT ADD add_exp_list PAR_RIGHT { $$ = $3; }
    | PAR_LEFT MUL mul_exp_list PAR_RIGHT { $$ = $3; }
;

add_exp_list:
    /* nothing */ { $$ = 0; }
    | add_exp_list exp { $$ = $1 + $2; }
;

mul_exp_list:
    /* nothing */ { $$ = 1; }
    | mul_exp_list exp { $$ = $1 * $2; }
;


%%


int main(int argc, char **argv) {
    printf("> ");
    yyparse();
    return 0;
}

void yyerror(char *s, ...) {
    va_list ap;
    va_start(ap, s);
    fprintf(stderr, "Line %d: ", yylineno);
    vfprintf(stderr, s, ap);
    fprintf(stderr, "\n");
}
